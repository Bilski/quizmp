
import io.mockk.MockKAnnotations
import model.Photo
import model.Quiz
import org.amshove.kluent.shouldBe
import kotlin.test.BeforeTest
import kotlin.test.Test

class QuizTest {

    private val quiz = Quiz("title", "content",
        Photo("", ""))


    @BeforeTest
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun testExample() {
        quiz.returnSikalafa() shouldBe "sikalafa"
    }

    @Test
    fun testExample2() {
        quiz.returnSikalafa() shouldBe "sikalafa2"
    }
}