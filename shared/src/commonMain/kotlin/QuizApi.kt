package api

import ApplicationDispatcher
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import model.Quiz
import model.QuizList

class QuizApi {

    private val httpClient = HttpClient()

    fun getQuizzes(success: (List<Quiz>) -> Unit, failure: (Throwable?) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val url = "http://quiz.o2.pl/api/v1/quizzes/0/100"
                val json = httpClient.get<String>(url)
                Json.nonstrict.parse(QuizList.serializer(), json)
                    .items
                    .also(success)
            } catch (ex: Exception) {
                failure(ex)
            }
        }
    }
}