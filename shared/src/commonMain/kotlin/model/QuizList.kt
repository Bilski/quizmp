package model

import kotlinx.serialization.Serializable

@Serializable
data class QuizList(
    val items: List<Quiz>
)