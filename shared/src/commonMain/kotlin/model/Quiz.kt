package model

import kotlinx.serialization.Serializable

@Serializable
data class Quiz(
    val title: String,
    val content: String,
    val mainPhoto: Photo
) {
    fun returnSikalafa() = "sikalafa"
}
