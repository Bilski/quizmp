package model

import kotlinx.serialization.Serializable

@Serializable
data class Photo(
    val url: String,
    val source: String
)