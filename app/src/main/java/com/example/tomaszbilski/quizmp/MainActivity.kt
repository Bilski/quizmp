package com.example.tomaszbilski.quizmp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import api.QuizApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = job + Main

    private val adapter = QuizListAdapter()

    private lateinit var api: QuizApi
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        api = QuizApi()
        job = Job()

        setupRecyclerView()
        loadQuizzes()
    }

    private fun setupRecyclerView() {
        listingView.layoutManager = LinearLayoutManager(this)
        listingView.adapter = adapter
    }

    private fun loadQuizzes() {
        api.getQuizzes(
            success = {
                launch(Main) { adapter.updateData(it)
                }},
            failure = { }
        )
    }




}
