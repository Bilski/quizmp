package com.example.tomaszbilski.quizmp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.quiz_view.view.*
import model.Quiz

class QuizListAdapter : RecyclerView.Adapter<QuizListAdapter.QuizHolder>() {

    private val quizList: ArrayList<Quiz> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizHolder =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.quiz_view, null)
            .let { QuizHolder(it) }

    override fun getItemCount(): Int =
        quizList.count()

    override fun onBindViewHolder(holder: QuizHolder, position: Int) =
        holder.bind(quizList[position])

    fun updateData(list: List<Quiz>) {
        quizList.clear()
        quizList.addAll(list)
        notifyDataSetChanged()
    }

    inner class QuizHolder(private val view: View): RecyclerView.ViewHolder(view), View.OnClickListener {

        override fun onClick(v: View?) { }

        fun bind(quiz: Quiz){
            view.quizTitle.text = quiz.title
            Glide.with(this.view.context)
                .load(quiz.mainPhoto.url)
                .into(view.quizThumbnail)
        }
    }

}
