//
//  ViewController.swift
//  KotlinIOS
//
//  Created by Tomasz Bilski on 14/07/2019.
//  Copyright © 2019 Tomasz Bilski. All rights reserved.
//

import UIKit
import shared
import SDWebImage

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    private let api: QuizApi = QuizApi()
    private var quizzes: [Quiz] = [Quiz]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        fetchQuizzes()
    }
    
    private func fetchQuizzes() {
        api.getQuizzes(success: { [weak self] quizzes in
            self?.quizzes.append(contentsOf: quizzes)
            self?.tableView.reloadData()
        }) { _ in
            // tutaj powinna byc obsluga bledow
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quizzes.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "quizItem") as! QuizListTableViewCell
        
        let quiz = quizzes[indexPath.row]
        
        cell.title.text = quiz.title
        cell.photo!.sd_setImage(with: URL(string: quiz.mainPhoto.url), completed: nil)
        
        return cell
    }
}

