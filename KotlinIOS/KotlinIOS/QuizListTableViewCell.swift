//
//  QuizListTableViewCell.swift
//  KotlinIOS
//
//  Created by Tomasz Bilski on 14/07/2019.
//  Copyright © 2019 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

class QuizListTableViewCell : UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var photo: UIImageView!
}
